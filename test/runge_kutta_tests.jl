using Base.Test
using FluxReconstruction

## Solution to coupled first order ode
# u' = 3u + 4v
# v' = -4u + 3v

rk_test_f(u, t) = [3 4; -4 3]*u;
u0 = [0.,1.]
dt = 0.01
t_end = 5.
u_num, t_num = rk_solve(u0, rk_test_f, dt, t_end, RK4)
y_exact(t) = [sin(4t)*exp(3t), cos(4t)*exp(3t)]
y_comp = hcat(y_exact.(t_num)...)

@test sum(abs.(hcat(u_num...) - y_comp)./(y_comp+1))/length(t_num) < 1e-5

