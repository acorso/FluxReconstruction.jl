using Base.Test
using FluxReconstruction
using Jacobi
using StaticArrays

# Test the LegendreElement functionality
el = LegendreElement(3, 3)
@test isa(el, LagrangeElement)
@test isa(el, NonInterfaceElement)
@test !isa(el, InterfaceElement)
@test solution_points(el) == broadcast_points(el.pts, 3)
@test solution_points(el, 2, 3) == broadcast_points(el.pts, 2)
@test solution_points(el, 2) == broadcast_points(el.pts, 1)
@test el.dim == 3
@test el.order == 3
@test el.npts == (4,4,4)
@test el.pts == legendre_zeros(4)
@test size(el.interface_mat) == (4,2)
@test size(el.deriv_mat) == (6,4)
@test el.w == wgj(zgj(4))
@test !on_interface(el)
e2 = to_interface(el)
@test isa(e2, InterfaceElement)
@test on_interface(e2)

# Test the divergence functions
flux(u) = u+1
usol = @SArray zeros(4, 4)
u = interface_vals(el, usol)
@test size(u) == (4,2)
@test isa(u, SArray)
f_vec = flux_vec(el, 4)
@test size(f_vec) == (4,6)
@test isa(f_vec,MArray)
flux_vals!(f_vec, el, usol, flux)
@test f_vec == toSArray([0.0 1.0 1.0 1.0 1.0 0.0; 0.0 1.0 1.0 1.0 1.0 0.0; 0.0 1.0 1.0 1.0 1.0 0.0; 0.0 1.0 1.0 1.0 1.0 0.0])




# Test the LobattoElement functionality
el = LobattoElement(3,2)
@test isa(el, LagrangeElement)
@test !isa(el, NonInterfaceElement)
@test isa(el, InterfaceElement)
@test solution_points(el) == broadcast_points(el.pts, 2)
@test solution_points(el, 2) == broadcast_points(el.pts, 1)
@test el.dim == 2
@test el.order == 3
@test el.npts == (4,4)
@test el.pts == zglj(4)
@test el.w == wglj(zglj(4))
@test size(el.deriv_mat) == (4,4)
@test on_interface(el)

# Test the divergence functions
flux(u) = u+1
usol = @SArray zeros(4, 4)
u = interface_vals(el, usol)
@test size(u) == (4,2)
@test isa(u, SArray)
f_vec = flux_vec(el, 4)
@test size(f_vec) == (4,4)
@test isa(f_vec,MArray)
flux_vals!(f_vec, el, usol, flux)
@test f_vec == toSArray([0.0 1.0 1.0 0.0; 0.0 1.0 1.0 0.0; 0.0 1.0 1.0 0.0; 0.0 1.0 1.0 0.0])

