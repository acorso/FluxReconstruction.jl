using Base.Test
using FluxReconstruction

xpts = [0, 0.25, 0.5, 0.75, 1]
ypts = [1 2 5 -1 0.5]
x = [-.25, xpts..., 1.25]

# Create 1D interpolates
p = lagrange_polynomial(xpts, ypts)
l = lagrange_basis(xpts)
dp = lagrange_polynomial_derivative(xpts, ypts)
dl = lagrange_basis_derivative(xpts)
m = interp_matrix(xpts, x)
dm = deriv_interp_matrix(xpts, x)

# Check that interpolation matches at samplepoints
@test all(p.(xpts)' .== ypts)

# Check that interpolation and derivative matches known values
@test p(-0.25) == 40.5
@test dp(-0.25) == -317.83333333333337
@test p(1.25) == 53.5
@test dp(1.25) == 401.1666666666667

# Check that the basis functions are working
@test all([dot(l(xpt), ypts) for xpt in xpts]' .== ypts)
@test size(l(xpts[1])) == size(xpts)
@test all([dot(dl(xpt), ypts) for xpt in xpts] .== dp.(xpts))
@test size(dl(xpts[1])) == size(xpts)

# Check that function matches interp matrices
@test all(ypts*m .== p.(x)')
@test sum(abs.(ypts*dm .- dp.(x)')) < 1e-13

## Check the 2d functionality
# Construct 2D interpolates
xpts_2d = broadcast_points(xpts, 2)
ypts_2d = rand(1,25)
p2d = lagrange_polynomial(xpts, ypts_2d, 2)
dp2d = lagrange_polynomial_derivative(xpts, ypts_2d, 2)
l = lagrange_basis(xpts, 2)
dl = lagrange_basis_derivative(xpts,2)

# Check the output of the gradient
@test isa(dp2d(1.0,1.0), Array{Float64,1})
@test length(dp2d(1.0,1.0)) == 2

# Check that the sample points match
@test reshape([p2d(x1,x2) for x1 in xpts, x2 in xpts],1,:) == ypts_2d

# Check that the interpolation matrix is working
# NOTE: deriv interp matrix is only implemented in 1D
m2 = interp_matrix(xpts, broadcast_points(x, 2))
@test sum(abs.(ypts_2d*m2 .- reshape([p2d(x1,x2) for x1 in x, x2 in x],1,:))) < 1e-12

# Check that the basis functions are working
@test all([dot(l(xpts_2d[:,i]...), ypts_2d) for i=1:size(xpts_2d,2)]' .== ypts_2d)
@test size(l(xpts_2d[:,1]...)) == (size(xpts_2d,2),)

@test isapprox([squeeze(ypts_2d * dl(xpts_2d[:,i]...), 1) for i=1:size(xpts_2d,2)], [dp2d.(xpts_2d[:,i]...) for i=1:size(xpts_2d, 2)])
@test size(dl(xpts_2d[:,1]...)) == size(xpts_2d')

