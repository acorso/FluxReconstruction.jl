using Base.Test
using FluxReconstruction
using StaticArrays

# Tests for the PointDict and PointSet
p1 = [1.1, 1.2, 1.3]
p2 = [2.1, 2.2, 2.3]
p31 = [2.10000000000001, 2.2, 2.3]
p4 = [2.10001, 2.2, 2.3]

dict, set = PointDict(), PointSet()

@test get(dict, p1, 0) == 0
@test_throws KeyError dict[p1]
@test !in(p1, set)

dict[p1], dict[p2]  = 1, 2
push!(set, p1 ,p2)

@test dict[p1] == 1 && dict[p2] == 2
@test in(p1, set) && in(p2, set)

dict[p31], dict[p4] = 4, 5
push!(set, p31, p4)

@test length(dict) == 3 && length(set) == 3
@test dict[p2] == dict[p31] && dict[p2] == 4

# gind tests
@test gind((1,1,1), (5,5,5)) == 1
@test gind((1,2,2), (5,5,5)) == 31

# exclusive sampling
samps = xsample(0,1,10)
@test samps[end] != 1
@test samps[1] == 0
@test length(samps) == 10

# Converting to a static array
@test isa(toSArray((1,2,3)), StaticArray)
@test isa(toSArray([1 2 3; 4 5 6]), StaticArray)

# A times A'
@test AxAT([1 2 3; 4 5 6]) == [14 32; 32 77]

# broadcast_points
x1,x2,x3 = -1.0, 0., 1.0
sol_pts = [x1 x1; x2 x1; x3 x1; x1 x2; x2 x2; x3 x2; x1 x3; x2 x3; x3 x3]'
@test sol_pts == broadcast_points([x1, x2, x3], 2)

