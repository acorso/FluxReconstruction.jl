using Base.Test
using FluxReconstruction

#NOTE: The functions: solution_points, reconstruct are "tested" through examples

m = LagrangeMesh(((0.,0.),(1.,1.)), (4,4), LegendreElement(3,2))
m2 = LagrangeMesh((0,0), (1,1), (4,4), LobattoElement(3,2))

@test isa(m.element, NonInterfaceElement)
@test isa(m2.element, InterfaceElement)

@test m.dim == m2.dim
@test m.range == m2.range
@test m.nel == m2.nel
@test m.h == m2.h
@test m.J == m2.J


@test m.dim == 2
@test m.range == ((0.,0.),(1.,1.))
@test m.nel == (4,4)
@test m.h == (0.25, 0.25)
@test m.J == (8., 8.)
@test m.element.dim == 2
@test m.element.order == 3

# Test for counting the number of points in the mesh
@test num_solution_points(m, 1) == 16
@test num_solution_points(m, 1, 2) == 16*16
@test num_solution_points(m) == 16*16

# Test for global_solution_points
@test to_global([-1., 1.0], (1,), (1.,), (0.,)) == [0., 1.]
rpts = rand(2,3)
@test to_global(rpts, (2,), m) == to_global(rpts, (2,), m.h, m.range[1])

# Test the sampling functionality
@test sample((x...)->[1.0, 1.0], m) == ones(2, m.element.npts..., m.nel...)

