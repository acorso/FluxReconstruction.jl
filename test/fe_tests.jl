using Base.Test
using FluxReconstruction

# Test 1D FEM against exact solution
L = 30;
m = LagrangeMesh((0,), (L,), (10,), LobattoElement(4,1))
x = solution_points(m)
bforce = reshape(sin.(2*pi*x / L), 5, 10)
ps1 = ProblemStatement((Dirichlet, 0))

sol = solve_diffusion(assemble_fem(m, ps1)..., m, bforce)

sol_vec =  [sol(a,e) for a=1:prod(m.element.npts), e=1:prod(m.nel)][:]
u_ex = (L^2/(4*pi^2))*sin.(2*pi*x/L)[:]
@test mean(abs.(sol_vec - u_ex)) < 1e-6

