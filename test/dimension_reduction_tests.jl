using FluxReconstruction
using Base.Test

# Integrate a solution vector from 2D to 1D
m = LagrangeMesh((0,0), (1,1), (10,10), LegendreElement(3,2))
f(x,y) = [y]
u = sample(f,m)
u_new = allocate_with_less_dims(size(u,1), m.element.npts, m.nel, 2)
integrate!(u_new, u, m, 2)
@test u_new == 0.5*ones(1,4,10)

