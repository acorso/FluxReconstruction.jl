using Base.Test
using FluxReconstruction

# 1D Checks
ps1 = ProblemStatement((Dirichlet, 0))
@test ps1.bflag == reshape([Dirichlet, Dirichlet], 2,1)
@test ps1.bval[MIN,1]() == 0
@test ps1.bval[MAX,1]() == 0

ps2 = ProblemStatement(((Dirichlet, Neumann), (0,1)))
@test ps2.bflag == reshape([Dirichlet, Neumann], 2,1)
@test ps2.bval[MIN,1]() == 0
@test ps2.bval[MAX,1]() == 1

ps3 = ProblemStatement(((Dirichlet, Neumann), (()->0,()->1)))
@test ps3.bflag == reshape([Dirichlet, Neumann], 2,1)
@test ps3.bval[MIN,1]() == 0
@test ps3.bval[MAX,1]() == 1

# 2D Checks
ps4 = ProblemStatement(((Dirichlet, 0), (Neumann, 1)))
@test ps4.bflag == [Dirichlet Neumann; Dirichlet Neumann]
@test ps4.bval[MIN,1]() == 0
@test ps4.bval[MIN,2]() == 1
@test ps4.bval[MAX,1]() == 0
@test ps4.bval[MAX,2]() == 1

ps5 = ProblemStatement((((Dirichlet, Neumann), (()->1,2)), ((Neumann, Dirichlet), (3,()->4))))
@test ps5.bflag == [Dirichlet Neumann; Neumann Dirichlet]
@test ps5.bval[MIN,1]() == 1
@test ps5.bval[MIN,2]() == 3
@test ps5.bval[MAX,1]() == 2
@test ps5.bval[MAX,2]() == 4

ps6 = ProblemStatement(2, (Dirichlet, 0))
@test ps6.bflag == [Dirichlet Dirichlet; Dirichlet Dirichlet]
@test ps6.bval[MIN,1]() == 0
@test ps6.bval[MIN,2]() == 0
@test ps6.bval[MAX,1]() == 0
@test ps6.bval[MAX,2]() == 0

