using Base.Test
using FluxReconstruction
using Jacobi

# Test that the quad points are in the right place
z = zgj(3,0,0)
w = wgj(z, 0, 0)
@test quad_pts(3, 1) == (reshape(z,1,:),w)

# Boundary Quadrature
zb, wb = boundary_quad_pts(3, 1, 1, MIN)
@test zb[1] == -1.0
@test wb[1] == 1.0


dim = 2
z2, w2 = quad_pts(3,dim)
@test isapprox(sum(w2), 2.0^dim)
@test z2 == broadcast_points(z, 2)

# Test that the numerical integration is working properly

# 1D
p = lagrange_polynomial([0.,1.], [0.,1.])
@test isapprox(quad_integrate(p, 1), 0.)
@test isapprox(quad_integrate(p, 4), 0.)

p2 = lagrange_polynomial(rand(6), rand(6))
@test isapprox(quad_integrate(p2, 6), quad_integrate(p2, 3))

p3(x) = 0.5*x + 0.5
@test isapprox(quad_integrate(p3, 3, 1, 2.), 0.5)

# 2D
p = lagrange_polynomial([0.,1.], [0.,0.5, 0.5, 1.0], 2)
p2dof(x...) = [p(x...), p(x...)]
quad_integrate(p2dof, 1, 2)
@test isapprox(quad_integrate(p2dof, 1, 2), [0., 0.])
@test isapprox(quad_integrate(p2dof, 3, 2), [0., 0.])

p2 = lagrange_polynomial(rand(6), rand(36), 2)
@test isapprox(quad_integrate(p2, 6, 2), quad_integrate(p2, 3, 2))

