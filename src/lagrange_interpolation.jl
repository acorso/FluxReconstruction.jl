# Resources for interpolating points using lagrange polynomials
using StaticArrays

export lagrange_basis, lagrange_basis_derivative, lagrange_polynomial,
lagrange_polynomial_derivative, interp_matrix, deriv_interp_matrix, heap_interp_matrix

"Returns the 1D Lagrange basis functions for the provided set of sample points"
function lagrange_basis(xpts::AbstractArray{Float64,1})
    function l(x::Float64)
        N = length(xpts)
        xnum = Array{Float64,1}(N-1)
        xden = Array{Float64,1}(N-1)
        lj_list = Array{Float64,1}(N)
        @inbounds for j=1:N
            xj = xpts[j]
            add::Int = 0;
            @inbounds for k=1:N
                if k == j
                    add = 1
                    continue
                end
                xk = xpts[k]
                index = k-add
                xnum[index] =  x - xk
                xden[index] = xj - xk
            end
            lj_list[j] = prod(xnum) / prod(xden)
        end
        return lj_list
    end
end

"Returns the 1D basis functions for the derivative of the lagrange polynomials
for the provided set of sample points"
function lagrange_basis_derivative(xpts::AbstractArray{Float64,1})
    function dl(x::Float64)
        N = length(xpts)
        xnum = Array{Float64,1}(N-2)
        xden = Array{Float64,1}(N-2)
        ljp_list = Array{Float64,1}(N)
         for j=1:N
            ljval = 0;
            xj = xpts[j];
             for i=1:N
                if j == i continue end
                xi = xpts[i]
                add::Int = 0
                for m=1:N
                    if m == i
                        add += 1
                        continue
                    end
                    if m == j
                        add += 1
                        continue
                    end
                    xm = xpts[m]
                    index = m-add
                    xnum[index] =  x - xm
                    xden[index] = xj - xm
                end
                ljval += prod(xnum)/(prod(xden)*(xj-xi))
            end
            ljp_list[j] = ljval
         end
         return ljp_list
    end
end

"Returns the ND lagrange basis function for the provided set of sample points."
function lagrange_basis(xpts::AbstractArray{Float64,1}, ndim::Int)
    l1d, N = lagrange_basis(xpts), length(xpts)
    function l(x::Float64...)
        @assert length(x) == ndim
        levals, l_list, g  = l1d.(x), ones(N^ndim), 1
        for inds in CartesianRange(ntuple(x->N, ndim))
            for n=1:ndim
                l_list[g] *= levals[n][inds[n]]
            end
            g += 1
        end
        return l_list
    end
end

"Returns the gradient of the ND lagrange baiss function for the provided sample points"
function lagrange_basis_derivative(xpts::AbstractArray{Float64}, ndim::Int)
    l1d, dl1d,  N = lagrange_basis(xpts), lagrange_basis_derivative(xpts), length(xpts)
    function dl(x::Float64...)
        @assert length(x) == ndim
        levals, dlevals, l_list  = l1d.(x), dl1d.(x), ones(N^ndim, ndim)
        for d=1:ndim
            g = 1
            for inds in CartesianRange(ntuple(x->N,ndim))
                for n=1:ndim
                    if n == d
                        l_list[g, d] *= dlevals[n][inds[n]]
                    else
                        l_list[g, d] *= levals[n][inds[n]]
                    end
                end
                g += 1
            end
        end
        return l_list
    end
end

"Returns the lagrange polynomial function that interpolates the provided points"
function lagrange_polynomial(xpts::AbstractArray{Float64,1}, ypts, ndim::Int...)
    l = lagrange_basis(xpts, ndim...)
    L(x::Float64...) = dot(ypts, l(x...))
end

"Returns the first derivative of the 1D lagrange polynomial that interpolates the supplied sample points"
function lagrange_polynomial_derivative(xpts::AbstractArray{Float64,1}, ypts, ndim::Int...)
    dl = lagrange_basis_derivative(xpts, ndim...)
    dL(x::Float64...) = length(x) > 1 ?  squeeze(reshape(ypts,1,:) * dl(x...) , 1) : dot(ypts, dl(x...))
end

"Interpolation matrix for a fixed set of ND interpolated points"
function interp_matrix(xpts::AbstractArray{Float64, 1}, xinterp::AbstractArray{Float64,2})
    ndim = size(xinterp, 1)
    l = (ndim > 1) ? lagrange_basis(xpts, ndim) : lagrange_basis(xpts)
    Ni = size(xinterp,2)
    mat = Array{Float64, 2}(length(l(xinterp[:,1]...)), Ni)
    for i=1:Ni
        mat[:,i] = l(xinterp[:,i]...)
    end
    mat
end

"Interpolation matrix for a fixed set of 1D interpolated points"
interp_matrix(xpts::AbstractArray{Float64, 1}, xinterp::AbstractArray{Float64,1}) = interp_matrix(xpts, reshape(xinterp, 1, length(xinterp)))

"Interpolation matrix for the derivative of the 1D lagrange polynomial"
function deriv_interp_matrix(xpts::AbstractArray{Float64,1}, xinterp::AbstractArray{Float64, 1})
    l = lagrange_basis_derivative(xpts)
    Ni = length(xinterp)
    mat = Array{Float64,2}(length(xpts), Ni)
    for i=1:Ni
        mat[:,i] = l(xinterp[i])
    end
    mat
end
