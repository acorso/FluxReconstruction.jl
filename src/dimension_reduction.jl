export allocate_with_less_dims, integrate!, reconstruct

"Allocates a solution vector with the specified dimensions removed. The vector is not filled"
function allocate_with_less_dims(dofs::Int, npts::iTup, nel::iTup, dims::Int...)
    new_npts = deleteat!([npts...], dims)
    new_nel = deleteat!([nel...], dims)
    Array{Float64}(dofs, new_npts..., new_nel...)
end

"Integrates one or more dimensions of a solution vector on a mesh"
function integrate!(u_new::Array{Float64}, u::Array{Float64}, m::LagrangeMesh, dims::Int...)
    dims = [dims...]
    kd, dofs = deleteat!([1:m.dim...], dims), size(u,1)
    eind, nind = zeros(Int, m.dim), Array{Any,1}(m.dim)
    nind[:] = Colon()
    weights = broadcast_weights(m.element.w, length(dims)) / prod(m.J[dims])
    # Loop through each of the points in the new solution vector (omitting the integrated dimensions)
    for ek in CartesianRange(m.nel[kd]), nk in CartesianRange(m.element.npts[kd])
        acc = 0. # Initialize an accumulator to 0
        # Loop through each element in the integration dimensions and add the quadtrature contribution form that element
        for ei in CartesianRange(m.nel[dims])
            eind[kd], eind[dims], nind[kd] = [ek.I...], [ei.I...], [nk.I...]
            acc += reshape(u[:, nind..., eind...], dofs, :) * weights
        end
        u_new[:,  nk.I..., ek.I...,] = acc # assign accumulator to position
    end
end


"Reconstruct the solution using polynomial interpolation"
function reconstruct(m::LagrangeMesh, u::Array{Float64}, ni::Tuple, ei::Tuple, nsamp::Int)
    sd, fd = find(ei .== Colon()), find(ei .!= Colon()) # slice/fixed dimensions
    nsd = length(sd) # number of slice dimensions
    # Make sure the length of the indices match and are sliced in the same dimension
    @assert m.dim == length(ei) && sd == find(ni .== Colon()) && length(ni) == m.dim

    # If a 1dof solution, add another dim
    length(size(u)) < 2*m.dim + 1 && (u = reshape(u, 1, m.element.npts..., m.nel...))

    xsamp = broadcast_points(xsample(-1., 1., nsamp), nsd) # sample points in an element
    mat = interp_matrix(m.element.pts, xsamp) # interpolation matrix
    urec, eind = Array{Float64}(size(u,1), m.nel[sd].*nsamp... ), ones(Int,m.dim) # reconstructed solution and index for iterating over elements
    ndof = size(u,1)
    for es in CartesianRange(m.nel[sd]) # iterate over elements in the mesh
        eind[fd], eind[sd] = [ei[fd]...], [es.I...] # Reconstruct the element index
        uel = reshape(u[:, ni..., eind...], ndof, :)*mat # Get the element-sampled solution points
        urec[:,[(es.I[i] - 1)*nsamp + 1 : nsamp*es.I[i] for i=1:nsd]...] = uel

    end
    squeezeall(urec), [xsample(m.range[1][n], m.range[2][n], nsamp*m.nel[n]) for n=sd]...
end

"Reconstruct a mesh in all dimensions of the mesh"
reconstruct(m::LagrangeMesh, u::Array{Float64}, nsamp::Int) =
    reconstruct(m, u, ntuple(x->:, m.dim), ntuple(x->:,m.dim), nsamp)

