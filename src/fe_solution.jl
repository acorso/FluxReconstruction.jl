export assemble_fem, solve_diffusion

function solve_diffusion(K::SparseMatrixCSC{Float64}, Fbase::Array{Float64,1}, me::Array{Float64,2}, LM::Array{Int,2}, dir_mask::SparseMatrixCSC{Float64}, m::LagrangeMesh, f)
    F = add_rhs(f, Fbase, LM, m, me)
    u = K \ F
    construct_solution_fn(u, LM, dir_mask)
end

"Assemble the needed data structures to solve the FEM diffusion problem"
function assemble_fem(m::LagrangeMesh, ps::ProblemStatement)
    LM, dir_mask, neu_mask = location_matrix(m, ps)
    ke = element_stiffness(m.element, m.j)
    me = element_mass(m.element, m.j)
    bmass = boundary_mass(m.element, m.J)
    #TODO - Factorize K??
    K = assemble_stiffness(ke, LM, prod(m.nel), prod(m.element.npts))
    Fbase = assemble_force_base(m, LM, dir_mask, neu_mask, ps, ke, bmass)
    K, Fbase, me, LM, dir_mask
end

"Adds the body force contribution to the force vector (called each solve)"
function add_rhs(f, Fbase::Array{Float64,1}, LM::Array{Int,2}, m::LagrangeMesh, me::Array{Float64,2})
    Fnew = copy(Fbase)
    npts, nel = size(f,1), size(f,2)
    for e in 1:size(f,2)
        fe = me * f[:,e]
        for a=1:npts
            LM[a,e] != 0 && (Fnew[LM[a,e]] += fe[a])
        end
    end
    Fnew
end

"Construct function that returns the solution at element and node (with boundary)"
function construct_solution_fn(u::Array{Float64}, LM::Array{Int, 2}, dir_mask::SparseMatrixCSC{Float64})
    sol(a, e) = (LM[a,e] == 0) ? dir_mask[a,e] : u[LM[a,e]]
end

"Constructs and returns the element stiffness matrix for the provided element"
function element_stiffness(el::LagrangeElement, j::Float64)
    dl = lagrange_basis_derivative(el.pts, el.dim)
    quad_integrate((x...) -> AxAT(j*dl(x...)), el.npts[1]*2, el.dim, j)
end

"Constructs and returns the element mass matrix for the provided element"
function element_mass(el::LagrangeElement, j::Float64)
    l = lagrange_basis(el.pts, el.dim)
    quad_integrate((x...) -> AxAT(l(x...)), el.npts[1], el.dim, j)
end

"Constructs and returns a vector that is the shape function integrated on the specified boundary"
function boundary_mass(el::LagrangeElement, J::fTup)
    l = lagrange_basis(el.pts, el.dim)
    bmass = Array{Array{Float64, 1}, 2}(2, el.dim)
    for m = [MIN,MAX], d=1:el.dim
        bmass[m,d] = boundary_quad_integrate(l, el.npts[1], el.dim, J, d, m)
    end
    bmass
end

"Assemble the element stiffness matrix into the global sparse matrix"
function assemble_stiffness(ke::Array{Float64, 2}, LM::Array{Int,2}, nel, npts)
    neq = maximum(LM)
    K = spzeros(neq,neq)
    for e=1:nel, i=1:npts, j=1:npts
        LM[i,e] != 0 && LM[j,e] != 0 && (K[LM[i,e], LM[j,e]] += ke[i,j])
    end
    K
end

"Assembles and returns the global force vector from bcs"
function assemble_force_base(m::LagrangeMesh, LM::Array{Int, 2}, dir_mask::SparseMatrixCSC{Float64}, neu_mask::SparseMatrixCSC{Float64}, ps::ProblemStatement, ke::Array{Float64, 2}, bmass::Array{Array{Float64, 1}, 2})
    neq, npts, nel = maximum(LM), prod(m.element.npts), prod(m.nel)
    F = zeros(neq)
    for eijk in CartesianRange(m.nel)
        e = gind(eijk.I, m.nel)
        on_nmin = (ps.bflag[MIN, :].==Neumann) .& (eijk.I .== 1)
        on_nmax = (ps.bflag[MAX, :].==Neumann) .& (eijk.I .== m.nel)

        fe = -ke*full(dir_mask[:,e]) # Dirichlet
        # Neumann
        for i=find(on_nmin)
            fe += neu_mask[:, e].*bmass[MIN, i]
        end
        for i=find(on_nmax)
            fe += neu_mask[:, e].*bmass[MAX, i]
        end
        for i=1:npts
            LM[i, e] != 0 && (F[LM[i, e]] += fe[i])
        end
    end
    F
end


"Returns the location matrix and boundary masks of the mesh"
function location_matrix(m::LagrangeMesh, ps::ProblemStatement)
    highest_index, npts = 1, m.element.npts
    L = fill(-1, prod(npts), prod(m.nel))
    dir_mask, neu_mask = spzeros(size(L)...), spzeros(size(L)...)

    for eijk in CartesianRange(m.nel), aijk in CartesianRange(npts)
        g, ai, ei = highest_index, gind(aijk.I, npts), gind(eijk.I, m.nel)

        # Get flags for where the current point is on the mesh
        on_min = [(aijk.I .== 1) .& (eijk.I .== 1)...]
        on_max = [(aijk.I .== npts) .& (eijk.I .== m.nel)...]
        on_el_min = (aijk.I .== 1) .& .! on_min
        minb = [on_min[i] ? ps.bflag[MIN, i] : UndefinedBoundary for i=1:m.dim]
        maxb = [on_max[i] ? ps.bflag[MAX, i] : UndefinedBoundary for i=1:m.dim]

        # If it is on a boundary then handle those cases
        if any(on_min .| on_max)
            dmin, dmax = minb .== Dirichlet, maxb .== Dirichlet
            nmin, nmax = minb .== Neumann, maxb .== Neumann
            pmin, pmax = minb .== Periodic, maxb .== Periodic
            # Periodic Condition
            if any(pmin .| pmax)
                newa = gind(aijk.I .- (aijk.I .- 1).*pmax .+ (npts .- 1).*pmin, npts)
                newe = gind(eijk.I .- (eijk.I .- 1).*pmax .+ (m.nel .- 1).*pmin, m.nel)
                g = L[newa, newe]
            end
            # Dirichlet Condition
            if any(dmin); dir_mask[ai,ei], g = ps.bval[MIN, dmin][1](), 0
            elseif any(dmax); dir_mask[ai,ei], g = ps.bval[MAX, dmax][1](), 0; end
            # Neumann Condition
            if any(nmin); neu_mask[ai,ei] = ps.bval[MIN, nmin][1]()
            elseif any(nmax); neu_mask[ai,ei] = ps.bval[MAX, nmax][1](); end
        end

        # If it is on a shared boundary, use the previously assigned index
        if any(on_el_min)
            newa = gind(aijk.I .+ (npts .- 1).*on_el_min, npts)
            newe = gind(eijk.I .- on_el_min, m.nel)
            g = L[newa, newe]
        end

        # If a global index was referenced but uninitialized, give it a new index
        g == -1 && (g = highest_index)
        # If the highest index was used, incremement it
        g == highest_index && (highest_index += 1)
        # Assign the global index (and dirichlet mask if needed)
        L[ai, ei] = g
    end
    L, dir_mask, neu_mask
end

