export create_divf, divf!, fr_error, fr_convergence

using StaticArrays

function create_divf(m::LagrangeMesh, p::ProblemStatement, dof::Int)
    divf_arr = Array{Float64}((dof, m.element.npts..., m.nel...))
    npts = num_solution_points(m)
    function divf(u::Array{Float64}, t::Float64)
        divf!(divf_arr, m, p, u, t)
        divf_arr
    end
end

"Fills in the divergence of the 1st order flux function"
function divf!(divf::Array{Float64}, m::LagrangeMesh, p::ProblemStatement, u::Array{Float64}, t::Float64)
    # allocate needed arrays
    npts, ndof = m.element.npts[1], size(u,1)
    f = flux_vec(m.element, ndof)
    uc_sol = @MArray zeros(ndof, npts)
    ur_sol = @MArray zeros(ndof, npts)
    for d=1:m.dim # This is the dimension along which the deriv will be taken
        fint, flux, od = p.interface_flux[d], p.flux[d], [1:d-1..., d+1:m.dim...]
        for jk=CartesianRange(m.element.npts[od]) # Loop over other dimensions
            li = (:,jk.I[1:d-1]...,:,jk.I[d:end]...) # Location in each element
            for ejk=CartesianRange(m.nel[od]) # Loop over elements
                cind = (li..., ejk.I[1:d-1]...,1,ejk.I[d:end]...) # Full index of solution points
                uc_sol[:,:] = u[cind...] # Solution in the first element
                uc = interface_vals(m.element, uc_sol) # Interface interpolation of first element

                #  Handle minimum boundary condition to get left flux
                if p.bflag[MIN,d] == Dirichlet
                    f[:,1] = fint(p.bval[MIN,d](t), uc[:,1])
                elseif p.bflag[MIN,d] == Periodic
                    ur_sol[:] = u[li...,ejk.I[1:d-1]...,end,ejk.I[d:end]...]
                    ur = interface_vals(m.element, ur_sol)
                    f[:,1] = fint(ur[:,2], uc[:,1])
                elseif p.bflag[MIN,d] == Neumann
                    f[:,1] = p.bval[MIN,d](t)
                end

                for i in 1:m.nel[d]-1 # Loop through all elements in the deriv direction
                    rind = (li..., ejk.I[1:d-1]...,i+1,ejk.I[d:end]...) # Index of the solution in the right element
                    ur_sol[:,:] = u[rind...] # Solution in right element
                    ur = interface_vals(m.element, ur_sol) # Interface solutions of right element
                    f[:, end] = fint(uc[:,2], ur[:,1]) # Interface flux to the right
                    flux_vals!(f, m.element, uc_sol, flux)
                    gradf = m.J[d] * (f * m.element.deriv_mat) # Derivative of flux in direction - d
                    (d == 1) ? divf[cind...] = gradf : divf[cind...] += gradf # set or add to divergence

                    # shift the frame
                    uc, uc_sol[:], cind, f[:,1] = ur, ur_sol[:], rind, f[:,end]
                end

                # Handle max boundary condition  to populate last element
                if p.bflag[MAX,d] == Dirichlet
                    f[:,end] = fint(uc[:,2], p.bval[MAX, d](t))
                elseif p.bflag[MAX, d] == Periodic
                    ur_sol[:] = u[li...,ejk.I[1:d-1]...,1,ejk.I[d:end]...]
                    ur = interface_vals(m.element, ur_sol)
                    f[:,end] = fint(uc[:,2], ur[:,1])
                elseif p.bflag[MAX, d] == Neumann
                    f[:,end] = p.bval[MAX, d](t)
                end
                # Compute the final deriv
                flux_vals!(f, m.element, uc_sol, flux)
                gradf = m.J[d] * f * m.element.deriv_mat
                (d == 1) ? divf[cind...] = gradf : divf[cind...] += gradf
            end
        end
    end
end

"Compute the relative error between the numerical and exact solution"
function fr_error(u0, m::LagrangeMesh, flux, interface_flux, u_exact)
    x = global_positions(m)
    f = create_dfdx_fr(m, flux, interface_flux)
    u,t = rk_solve(u0(x), f, 0.0001, .01, RK4)
    mean(abs.((u[:,end]-u_exact(x, t[end])')./u_exact(x, t[end])'))
end

"Compute the rate of decay of the relative error as the mesh size is reduced by factors 2"
function fr_convergence(u0, flux, interface_flux, Nel_start, u_exact, Nsol, steps = 4)
    N_els = [Nel_start*2.^(i-1) for i in 1:steps]
    meshes = [LagrangeMesh((0., 1.), Nel, Nsol) for Nel in N_els]
    Npts = N_els*Nsol
    err = [fr_error(u0,m,flux, interface_flux, u_exact ) for m in meshes]
    b,m = linreg(log10.(Npts), log10.(err))
    err, Npts, m, b
end

