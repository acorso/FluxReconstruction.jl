export PointDict, PointSet, fTup, iTup, gind, xsample, toSArray, AxAT, broadcast_points, squeezeall, err_reg

using DataStructures
using StaticArrays

"Defines a struct that will be used to set the ordering"
struct ApproxForwardOrdering <: Base.Ordering end

"Create a singleton for use in the ordering"
const ApproxForward = ApproxForwardOrdering()

"Extends the lt function for the case of approximate forward ordering and arrays"
Base.lt(::ApproxForwardOrdering, a::Array{Float64,1}, b::Array{Float64,1}) = isless(tuple(a...),tuple(b...)) && !all(isapprox.(a,b))

"Extends the equality function for approximate equality and arrays"
eq(::ApproxForwardOrdering, a::Array{Float64,1}, b::Array{Float64,1}) = all(isapprox.(a,b))

"Dictionary that maps points to indices (using approximate equality functions)"
PointDict() = SortedDict{Array{Float64,1}, Int}(ApproxForward)

"Set of points that uses the approximate equality function"
PointSet() = SortedSet{Array{Float64,1}}(ApproxForward)

"Type definition for variable size Float64 Tuple"
fTup = Tuple{Vararg{Float64}}

"Type definition for variable size Float64 Tuple"
iTup = Tuple{Vararg{Int}}

"Convert an ijk value to an index"
gind(ijk, n_pts) = dot([ijk.-1...], [1, cumprod([n_pts...])[1:end-1]...]) + 1

"Get the exclusive sampling (leaves out last point)"
xsample(start, stop, nsamp) = [linspace(start, stop, nsamp+1)...][1:end-1]

"Convert tuple to StaticArray"
toSArray(t::Tuple) = SArray{Tuple{length(t)}, Float64}(t)

"Convert Array type to StaticArray"
toSArray(v) = SArray{Tuple{size(v)...}, Float64}(v)

"Take a matrix and multiply by itself transpose"
AxAT(A) = A*A'

"Broadcasts an array of points into the specified dimension"
function broadcast_points(pts1D::AbstractArray{Float64,1}, dim::Int)
    npts = ntuple((x)->length(pts1D), dim)
    pts, g = Array{Float64,2}(dim, prod(npts)), 0
    for inds in CartesianRange(npts)
        pts[:, g+=1] = pts1D[[inds.I...]]
    end
    pts
end

"squeeze all dims that are 1"
squeezeall(a) = squeeze(a,(find(size(a).==1)...))

"Get the logarithmic regression line and slope of error"
function err_reg(xpts, err, npts=100)
    flip = xpts[2] < xpts[1]
    b,m = linreg(log10.(xpts), log10.(err))
    x = logspace(extrema(log10.(xpts))...,npts)
    y = 10^b*x.^m
    x,y,m
end

