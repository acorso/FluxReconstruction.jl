export LagrangeElement, InterfaceElement, NonInterfaceElement, LobattoElement, LegendreElement, InterfaceLegendreElement
export solution_points, on_interface, to_interface, interface_vals, flux_vec, flux_vals!

using StaticArrays
using Jacobi

abstract type LagrangeElement end

"Returns an array of solution points within an element"
solution_points(el::LagrangeElement) = broadcast_points(el.pts, el.dim)

"Returns an array of solution points with the specified dimensions"
solution_points(el::LagrangeElement, d1::Int, d::Int...) = broadcast_points(el.pts, length(d) + 1)

"Returns true if the element has interface points as part of the solution points"
on_interface(el::LagrangeElement) = isa(el, InterfaceElement)

############################################################################
#                            Legendre Element                              #
############################################################################

"Struct that defines a lagrangian element with legendre-zero solution points"
struct NonInterfaceElement <: LagrangeElement
    "The order of the element"
    order::Int
    "The number of spatial dimensions of the element"
    dim::Int
    "Number of solution points in each dimension"
    npts::iTup
    "The location of the solution points in 1D"
    pts::SArray
    "Quadtrature weights for solution points"
    w::SArray
    "The interpolation matrix for computing interface solution using solution points"
    interface_mat::SArray
    "The interpolation matrix for computing the derivative at solution points using all points"
    deriv_mat::SArray

end

"Constructor for a LegendreElement"
function LegendreElement(order::Int, dim::Int)
    z = zgj(order+1)
    NonInterfaceElement(toSArray(z), toSArray(wgj(z)), order, dim)
end

"Constructor for a NonInterfaceElement"
function NonInterfaceElement(pts::SArray, w::SArray, order::Int, dim::Int)
    ipts = [-1., 1.] # Location of 1D interface points
    npts = ntuple(x-> order+1, dim) # The number of solution points in each dimension
    int_mat = toSArray(interp_matrix(pts, ipts)) # Matrix for solution interpolation
    deriv_mat = toSArray(deriv_interp_matrix([ipts[1], pts..., ipts[2]], pts)) # Matrix for derivative interpolation (with interface points)

    NonInterfaceElement(order, dim, npts, pts, w, int_mat, deriv_mat)
end

interface_vals(el::NonInterfaceElement, u_sol) = u_sol * el.interface_mat

flux_vec(el::NonInterfaceElement, ndof) = @MArray zeros(ndof, el.npts[1]+2)

function flux_vals!(f, el::NonInterfaceElement, u_sol, flux_fn)
    for k=1:el.npts[1] # Flux at solution points
        f[:,k+1] = flux_fn(u_sol[:,k])
    end
end

"Creates a new element with the same points but also with the interface points"
to_interface(el::NonInterfaceElement) = InterfaceLegendreElement(el.order, el.dim)



############################################################################
#                            Interface Element                              #
############################################################################
"Struct that defines a lagrangian element with Gauss-Lobatto solution points"
struct InterfaceElement <: LagrangeElement
    "The order of the element"
    order::Int
    "The number of spatial dimensions of the element"
    dim::Int
    "Number of solution points in each dimension"
    npts::iTup
    "The location of the solution points in 1D"
    pts::SArray
    "Quadtrature weights for solution solution points"
    w::SArray
    "The interpolation matrix for computing the derivative at solution points using all points"
    deriv_mat::SArray
end

function LobattoElement(order::Int, dim::Int)
    z = zglj(order+1)
    InterfaceElement(toSArray(z), toSArray(wglj(z)), order, dim)
end

function InterfaceLegendreElement(order::Int, dim::Int)
    z = [-1., zgj(order+1)..., 1.]
    w = [0., wgj(zgj(order+1))..., 0.]
    InterfaceElement(toSArray(z), toSArray(w), order+2, dim)
end

function InterfaceElement(pts::SArray, w::SArray, order::Int, dim::Int)
    npts = ntuple(x-> order+1, dim) # The number of solution points in each dimension
    deriv_mat = toSArray(deriv_interp_matrix(pts, pts)) # Matrix for derivative interpolation (with interface points)

    InterfaceElement(order, dim, npts, pts, w, deriv_mat)
end

interface_vals(el::InterfaceElement, u_sol) = u_sol[:,@SArray [1,end]]

flux_vec(el::InterfaceElement, ndof) = @MArray zeros(ndof, el.npts[1])

function flux_vals!(f, el::InterfaceElement, u_sol, flux_fn)
    for k=2:el.npts[1]-1 # Flux at solution points
        f[:,k] = flux_fn(u_sol[:,k])
    end
end
