export Dirichlet, Neumann, Periodic, UndefinedBoundary, dirichlet, neumann, ProblemStatement, MIN, MAX

using StaticArrays
"Defines the boundary type"
@enum BoundaryType Dirichlet Neumann Periodic UndefinedBoundary

"Represents the minimum of a dimension (as an index)"
const MIN=1
"Represents the maximum of a dimension (as an index)"
const MAX=2

"Functions for boundary conditions"
return_constant(val) = c(args...) = val
dirichlet(val::Float64) = return_constant( @SArray [val])
dirichlet(val) = return_constant(val)
neumann(val::Float64) = return_constant(@SArray [val])
neumann(val) = return_constant(val)

"Defines a problem through the flux function, interface flux and boundary conditions"
struct ProblemStatement
    bflag::Array{BoundaryType, 2} # defines boundary type at min, max boundaries for each dim
    bval::Array{Function, 2} #defines the boundary condition functions
    flux::Array{Function,1} # array of flux functions (one for each dimension)
    interface_flux::Array{Function,1} # array of interface flux functions (one for each dim)
end

# TODO: Write explanation on how to use this
"Construct a ProblemStatement from nested tuples describing the boundary"
function ProblemStatement(bcs, flux = [], interface_flux = [])
    # Handle no tuple encapsulation
    if !isa(bcs[1], Tuple) || isa(bcs[1], NTuple{2, BoundaryType})
        bcs = tuple(bcs)
    end
    dim  = length(bcs)
    bval, bflag = Array{Function, 2}(2, dim), Array{BoundaryType, 2}(2, dim)
    for i=1:dim
        flags, vals = bcs[i][1], bcs[i][2]
        # Set the flags
        bflag[:,i] = (isa(flags, NTuple{2, BoundaryType})) ? [flags...] : flags

        # Set the boundaries
        if isa(vals, Tuple)
            assert(length(vals) == 2)
            bval[MIN, i] = isa(vals[1], Function) ? vals[1] : ()->vals[1]
            bval[MAX, i] = isa(vals[2], Function) ? vals[2] : ()->vals[2]
        else
            bval[:,i] = isa(vals, Function) ? vals : ()->vals
        end
    end
    ProblemStatement(bflag, bval, flux, interface_flux)
end

"Construct ProblemStatement that repeats boundary in each dim specified"
ProblemStatement(dim::Int, bcs, flux = [], interface_flux = []) = ProblemStatement(ntuple((i...)->bcs, dim), flux, interface_flux)

