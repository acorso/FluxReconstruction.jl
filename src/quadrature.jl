using Jacobi
export quad_pts, broadcast_weights, boundary_quad_pts, quad_integrate, boundary_quad_integrate

"Return the nd list of quadrature points and weights to be used for quadrature integration"
function quad_pts(order, dim)
    z = zgj(order, 0, 0)
    w = wgj(z, 0, 0)
    samps, weights, g = zeros(dim, order^dim), zeros(order^dim), 1
    for ijk = CartesianRange(ntuple(x->order, dim))
        i = [ijk.I...]
        samps[:,g], weights[g] = z[i], prod(w[i])
        g += 1
    end
    samps, weights
end

"Broadcast quadtrature weights to higher dimensions"
function broadcast_weights(w, dim)
    order = length(w)
    weights, g = Array{Float64}(order^dim), 0
    for ijk = CartesianRange(ntuple(x->order, dim))
        weights[g += 1] = prod(w[[ijk.I...]])
    end
    weights
end

function el_quad_pts(order, pts)
    qpts = zgj(convert(Int,ceil(order/2)), 0, 0)
    quad_mat = toSArray(interp_matrix(pts, qpts))
    w = toSArray(wgj(qpts, 0, 0))
    quad_mat, w
end

"Return the nd list of quadrature points along the specified boundary for integration"
function boundary_quad_pts(order, dim, bdim, minmax)
    @assert minmax==MIN || minmax==MAX
    otherdims = [1:(bdim-1)..., bdim+1:dim...]
    bval = (minmax == MIN) ? -1. : 1.
    bsamps = fill(bval, (dim, order^(dim-1)))
    samps, weights = quad_pts(order, dim-1)
    for i=1:size(samps, 2)
        bsamps[otherdims, i] = samps[:,i]
    end
    bsamps, weights
end

"Integrates a function using ND gaussian quadrature"
function quad_integrate(f, order::Int, dim::Int = 1, j::Float64 = 1.)
    z, w = quad_pts(order, dim)
    sum([f(z[:,i]...).*w[i] for i in 1:length(w)])/j
end

"Integrates a function on the boundary of an element"
function boundary_quad_integrate(f, order::Int, dim::Int, J::fTup, bdim, minmax)
    z,w = boundary_quad_pts(order, dim, bdim, minmax)
    otherdims = [1:(bdim-1)..., bdim+1:dim...]
    j = prod(J[otherdims])
    sum([f(z[:,i]...).*w[i] for i in 1:length(w)])/j
end

