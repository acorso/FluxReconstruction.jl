export LagrangeMesh, solution_points, num_solution_points, sample, to_global

"Struct that defines a mesh made of Lagrangian elements"
struct LagrangeMesh
    "Dimension of the mesh"
    dim::Int
    "Range of the mesh"
    range::Tuple{fTup, fTup}
    "Number of elements in the mesh in each dimension"
    nel::iTup
    "Grid spacing in each dimension"
    h::fTup
    "Jacobian in each dimension"
    J::fTup
    "Jacobian determinant"
    j::Float64
    "Sample element in the mesh"
    element::LagrangeElement
end

"Create a LagrangeMesh from a range, number of elements, order and dimension"
function LagrangeMesh(range::Tuple{fTup, fTup}, nel::iTup, el::LagrangeElement)
    @assert length(range[1]) == length(nel)
    @assert length(nel) == el.dim
    dim = length(nel) # Extract the dimension from the arrays
    h = (range[2].-range[1]) ./ nel # Compute the grid spacing
    J = 2.0 ./ h # Compute the determinant
    j = prod(J) # Product for now (determinant when J is a matrix)
    LagrangeMesh(dim, range, nel, h, J, j, el)
end

"Constructor for LagrangeMesh with min/max description of range"
function LagrangeMesh(min_pt::Tuple, max_pt::Tuple, nel::iTup, el::LagrangeElement)
    range = (convert.(Float64,min_pt), convert.(Float64,max_pt))
    LagrangeMesh(range, nel, el)
end

"Get the total number of solution points in the specified dimensions"
num_solution_points(m::LagrangeMesh, d1::Int, d::Int...) = prod(m.nel[[d1, d...]])*prod(m.element.npts[[d1, d...]])

"Get the total number of solution points in the mesh"
num_solution_points(m::LagrangeMesh) = num_solution_points(m, 1:m.dim...)

"Convert points relative to an element into global space"
to_global(pts::Array{Float64}, ei::iTup, h::fTup, s::fTup) = s .+ h.*(ei.-1) .+ h.*(pts+1)/2

"Convert points relative to an element into global space"
to_global(pts::Array{Float64}, ei::iTup, m::LagrangeMesh) = to_global(pts, ei, m.h, m.range[1])

"Sample a function onto the solution points of the mesh"
function sample(f, m::LagrangeMesh)
    ndof = length(f(m.range[1]...)) # Get the dofs of f
    u = Array{Float64, 2*m.dim+1}(ndof, m.element.npts..., m.nel...)
    for ei in CartesianRange(m.nel), ni in CartesianRange(m.element.npts)
        u[:,ni.I...,ei.I...] = f(to_global(m.element.pts[[ni.I...]], ei.I, m)...)
    end
    u
end

"Get a list of all solution points in N-Dimensions"
function solution_points(m::LagrangeMesh, d1::Int , d::Int...)
    darr, dim = [d1, d...], length(d)+1
    pts = Array{Float64,2}(dim, num_solution_points(m, d1, d...))
    spts = broadcast_points(m.element.pts, dim)
    er, g = prod(m.element.npts[darr]), 1

    for ei in CartesianRange(m.nel[darr])
        pts[:,(g-1)*er+1:g*er] = to_global(spts, (ei.I), (m.h[darr]...), (m.range[1][darr]...))
        g += 1
    end
    squeezeall(pts)
end

"Get the solution points for all dimensions in the mesh"
solution_points(m::LagrangeMesh) = solution_points(m, 1:m.dim...)
