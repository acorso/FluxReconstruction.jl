# Resources for arbitrary order RK timestepping schemes
# Background: Runge kutta methods are time integration methods for initial value problems of the form
# u' = f(t, u)
# They are of arbitrary order of accuracy but the higher the order, the larger the number of function evaluations are required.
# Note that RK schemes can be adaptive such that an error estimate is calculated and used to control the allowable step size at each stage.

export ButcherTableau, ForwardEuler, αFamily, MidpointMethod, HeunsMethod,
RalstonMethod, RK4, ThreeEighthsRule, RungeKuttaFehlberg
export rk_solve, rk_step, rk_solve_end
export rk_error, rk_convergence


"Define a butcher tableau for use in RK timestepping schemes"
struct ButcherTableau
    "Recursive Coefficients"
    a::Array{Float64}
    "Final Summation Coefficients"
    b::Array{Float64}
    "Temporal Coefficients"
    c::Array{Float64}
    "Number of stages in the RK scheme"
    stages::Int
    "Name of the RK scheme"
    name::String
end

"Constructor for the ButcherTableau with no name"
ButcherTableau(a, b, c) = ButcherTableau(a, b, c, length(b), length(b)*"-Stage RK")

"Constructor for the ButcherTableau with a name"
ButcherTableau(a, b, c, name::String) = ButcherTableau(a, b, c, length(b), name)

# TODO: Specify types in order to speed things up
# TODO: Benchmark to make sure this is efficient and optimized
# u_{n+1} = u_n + h*b*k'
# k_i = f(u_n + h*k_curr * a[i-1, 1:i-1]')
# u_{n} is a column vector with M rows
# f(u_n) is a column vector with M rows
# k_i = is a column with M rows
# k is a matrix with M ros and s columns (goes form 1 to s as it is constructed)
# b is a row vector of length M
# a is a matrix of size s-1xs-1
## Note: Can we use macros to generate code to ignore the zeros?
"Takes a step using the current state vector, u, the flux function, f, the time
step, dt, and the specified scheme"
function rk_step(u, f, t, dt, scheme::ButcherTableau)
    k = [f(u,t)]
    for i=1:scheme.stages-1
        push!(k, f(u + dt*sum(k[1:i].*scheme.a[i,1:i]), t + scheme.c[i]*dt))
    end
    u + dt*sum(k.*scheme.b'), t+dt
end

"Returns the time history of a solution from t=0 to t=t_end from the initial
condition u0, using the specified time integration scheme"
function rk_solve(u0, f, dt, t_end, scheme::ButcherTableau)
    u,t = [u0],[0.]
    c = 0
    while t[end] < t_end
        if c % 100 == 0
            println("t=", t[end])
        end
        us, ts = rk_step(u[end], f, t[end], dt, scheme)
        push!(u, us)
        push!(t, ts)
        c += 1
    end
    u,t
end

"Returns the end state of the RK solution"
function rk_solve_end(u0, f, dt, t_end, scheme::ButcherTableau)
    c,t = 0,0.
    u = u0
    while t < t_end
        if c % 100 == 0
            println("t=", t[end])
        end
        u = rk_step(u, f, t, dt, scheme)
        t += dt
        c += 1
    end
    u,t
end

"Computes the relative error between the numerical and exact solution"
function rk_error(u0, f, dt, t_end, scheme::ButcherTableau, u_exact)
    u,t = rk_solve(u0, f, dt, t_end, scheme)
    mean(abs.((u-u_exact.(t))./u_exact.(t)))
end

"Compute the rate of decay of the relative error as the timestep is reduced by 2"
function rk_convergence(u0, f, dt_start, t_end, scheme::ButcherTableau, u_exact, steps = 4)
    dts = [dt_start/2.^(i-1) for i in 1:steps]
    err = [rk_error(u0,f,dt,t_end,scheme,u_exact) for dt in dts]
    err, dts
end

#*************************************
#*Define some common butcher tableaus*
#*************************************
"Forward Euler, first-order time integration scheme"
ForwardEuler = ButcherTableau([], [1.], [0.], "Forward Euler")

"α-Family of second-order time integration scheme with specific name"
αFamily(α, name::String) = ButcherTableau([α], [1-1/(2*α) 1/(2*α)], [α], name);

"α-Family of second-order time integration scheme"
αFamily(α) = αFamily(α, "α-Family w/ α=$(α)")

"Second-order midpoint method time integration scheme"
MidpointMethod = αFamily(0.5, "Midpoint Method")

"Second-order Heun's method time integration scheme"
HeunsMethod = αFamily(1, "Heun's Method")

"Ralston's second-order time integration scheme"
RalstonMethod = ButcherTableau([2/3], [1/4 3/4], [0 2/3], "Ralston Method")

"RK4 fourth-order time integration scheme"
RK4 = ButcherTableau([1/2 0 0; 0 1/2 0; 0 0 1], [1/6 1/3 1/3 1/6], [1/2 1/2 1], "RK4")

"3/8 rule time integration scheme"
ThreeEighthsRule = ButcherTableau([1/3 0 0; -1/3 1 0; 1 -1 1],[1/8 3/8 3/8 1/8],[1/3 2/3 1], "3/8 Rule")

"Runge-Kutta-Fehlberg fourth/fifth order scheme for error estimation"
RungeKuttaFehlberg = ButcherTableau([1/4 0 0 0 0; 3/32 9/32 0 0 0; 1932/2197 -7200/2197 7296/2197 0 0; 439/216 -8 3680/513 -845/4104 0; -8/27 2 -3544/2565 1859/4104 -11/40],[16/135 0 6656/12825 28561/56430 -9/50 2/55; 25/216 0 1408/2565 2197/4104 -1/5 0],[1/4 3/8 12/13 1 1/2],"RKF45")

