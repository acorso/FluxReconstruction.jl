using Plots
using FluxReconstruction

m = LagrangeMesh((0,), (3,), (3,), LegendreElement(2,1))
m2 = LagrangeMesh((0,), (3,), (3,), InterfaceLegendreElement(2,1))
u = sample((x)->1, m2)
xpts = solution_points(m)
ypts = rand(size(xpts))


xpts2 = solution_points(m2)
ypts2 = 2*rand(size(xpts2))
ypts2 = reshape(ypts2, size(u))
ypts2[1,5,1] = ypts2[1,1,2]
ypts2[1,5,2] = ypts2[1,1,3]


yplot, xplot = reconstruct(m, ypts, 500)
yplot2, xplot2 = reconstruct(m2, ypts2, 500)

p = plot(xplot, yplot, label="Solution")
scatter!(xpts2, ypts2[:], label="Flux Points")
scatter!(xpts, ypts, label="Solution Points")
plot!(xplot2, yplot2, label="Flux")


title!("Flux Reconstruction")
xlabel!("X")

all_y = linspace(minimum(yplot2[:]), maximum(yplot2[:]), 100)
plot!(0*ones(100), all_y, color=:black, label="")
plot!(1*ones(100), all_y, color=:black, label="")
plot!(2*ones(100), all_y, color=:black, label="")
plot!(3*ones(100), all_y, color=:black, label="")


savefig("discontinuous_flux")

