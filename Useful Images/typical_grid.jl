using Plots
using FluxReconstruction

D = 1
V = 1
order = 4

xdims = [1:D...]
vdims = [D+1:D+V...]

xmin = zeros(D)
xmax = 5*ones(D)
vmin = -3*ones(V)
vmax = 3*ones(V)
xel = 5*ones(Int, D)
vel = 3*ones(Int, D)

m = LagrangeMesh((xmin...,vmin...), (xmax..., vmax...), (xel..., vel...), LegendreElement(4, V+D))

mesh_pts = solution_points(m)
scatter(mesh_pts[1,:], mesh_pts[2,:], label="", markercolor=:black, markersize=2, size=(600,200))

allx = linspace(0,5,100)
allv = linspace(-3,3,100)

plot!(allx, (x)->-3, linewidth=3, color=:blue, label="")
plot!(allx, (x)->-1, linewidth=3, color=:blue, label="")
plot!(allx, (x)->1, linewidth=3, color=:blue, label="")
plot!(allx, (x)->3, linewidth=3, color=:blue, label="")

plot!(0*ones(100), allv, linewidth=3, color=:blue, label="")
plot!(1*ones(100), allv, linewidth=3, color=:blue, label="")
plot!(2*ones(100), allv, linewidth=3, color=:blue, label="")
plot!(3*ones(100), allv, linewidth=3, color=:blue, label="")
plot!(4*ones(100), allv, linewidth=3, color=:blue, label="")
plot!(5*ones(100), allv, linewidth=3, color=:blue, label="")

xlabel!("X")
ylabel!("V")
title!("Phase Space Discretization")
savefig("phase_space")



