using FluxReconstruction
using Plots

L = 30;
ps = ProblemStatement((Dirichlet, 30))
order_list = [1,2,4]
nel_list = [10,20,40,60]
err = Array{Float64}(length(nel_list), length(order_list))
for o=1:length(order_list)
    for n=1:length(nel_list)
        order, nel = order_list[o], nel_list[n]
        m = LagrangeMesh((0,), (L,), (nel,), LobattoElement(order,1))
        x = solution_points(m)
        bforce = reshape(sin.(2*pi*x / L), m.element.npts..., m.nel...)
        sol = solve_diffusion(assemble_fem(m, ps)..., m, bforce)

        u = [sol(a,e) for a=1:prod(m.element.npts), e=1:prod(m.nel)][:]
        u_ex = 30 + (L^2/(4*pi^2))*sin.(2*pi*x/L)

        err[n, o] = mean(abs(u-u_ex)./u_ex)
    end
end

p = plot()
colors = distinguishable_colors(length(order_list), RGB(1.0,.75,0))
title!("Convergence of FEM")
xlabel!("Number of Elements")
ylabel!("Error")
for o=1:length(order_list)
    x,y,m = err_reg(nel_list, err[:,o])
    scatter!(nel_list, err[:,o], label=string("El Order: ", order_list[o]+2, " (slope= ",@sprintf("%.2f",m) ,")" ), xscale=:log10, yscale=:log10, markercolor=colors[o])
    plot!(x,y, label="", linecolor=colors[o], legend = :bottomleft)
end
display(p)
savefig("FE_Convergence")

