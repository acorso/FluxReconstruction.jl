using FluxReconstruction
using Plots

L = 20
T = 24
ps = ProblemStatement((Dirichlet, ((t)->sin(pi*t/2), neumann(0.) )), [(u) -> -u], [(uL, uR) -> -uL])
order_list = [3]
nel_list = [20, 40]

err_list = Array{Float64,2}(length(nel_list), length(order_list))
u_res = Array{Array{Float64},2}(length(nel_list), length(order_list))
meshes = Array{LagrangeMesh,2}(length(nel_list), length(order_list))
for o = 1:length(order_list)
    for n = 1:length(nel_list)
        order = order_list[o]
        nel = nel_list[n]
        println("order: ", order, " nel: ", nel)

        m = LagrangeMesh((0,), (L,), (nel,), LegendreElement(order,1))
        xsol = solution_points(m)
        u = rk_solve(sample((x)->0,m), create_divf(m, ps, 1), 0.0001, T, MidpointMethod)[1][end]
        # err = mean(abs.(u[:] + sin.(pi*xsol/2)))
        err = mean(abs.(u[:][xsol .< 2] - u[:][(xsol .>= 16) .& (xsol .<= 18)]))
        println("err: ", err)

        # Store the results
        u_res[n,o], meshes[n,o], err_list[n,o] = u, m, err

        println("done")
    end
end

# err_list
# u = u_res[3,3]
# m = meshes[3,3]
#
# urec, xrec = reconstruct(m, u, 20)
#
# x = linspace(0,20,1000);
# p = plot(x, -sin.(pi*x/2), label="Initial Condition")
# plot!(xrec, urec, label="t=5")
# scatter!(solution_points(m), u[:])
# scatter!(linspace(0,20,m.nel[1]+1), zeros(m.nel[1]+1))
# Error is:
#  0.000242832
# 2.0539e-6
err_list = [0.000242832, 2.0539e-6]
p = plot()
for o = 1:length(order_list)
    x,y,m = err_reg(nel_list, err_list[:,o])
    scatter!(nel_list, err_list[:,o], xscale=:log10, yscale=:log10, markercolor=:blue, label="")
    plot!(x, y, label=string("m=",m), linecolor=:blue)
end
display(p)

xlabel!("Number of Elements")
ylabel!("Relative Error")
title!("High Frequency Convergence\n on third-order element")
savefig(p,"high_frequency_convergence")

