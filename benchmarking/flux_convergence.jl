using FluxReconstruction
using Plots

domain = ((-1.,),(10.,))
ps = ProblemStatement((Periodic, (dirichlet(0.))), [(u) -> -u], [(uL, uR) -> -uL])

ic(x) = exp(-20*(x)^2)
dfdx_exact_fn(x) = 40*x*exp(-20*x^2)

# ic(x) = sin(pi*x)
# dfdx_exact_fn(x) = -pi*cos(pi*x)


order_list = [1, 3, 4, 5, 6, 7, 8]
nel_list = [20, 50, 100]

err_list = Array{Float64,2}(length(nel_list), length(order_list))
u_res = Array{Array{Float64},2}(length(nel_list), length(order_list))
meshes = Array{LagrangeMesh,2}(length(nel_list), length(order_list))
for o = 1:length(order_list)
    for n = 1:length(nel_list)
        order, nel = order_list[o], nel_list[n]
        println("order: ", order, " nel: ", nel)

        m = LagrangeMesh(domain, (nel,), LegendreElement(order,1))
        u0 = sample(ic, m)
        divf = create_divf(m, ps, 1)
        dfdx_num = divf(u0, 0.)
        err = mean(abs.(dfdx_num .- sample(dfdx_exact_fn, m) ))
        println("err: ", err)

        # Store the results
        u_res[n,o], meshes[n,o], err_list[n,o] = dfdx_num, m, err

        println("done")
    end
end

u = u_res[2,3]
m = meshes[2,3]

urec, xrec = reconstruct(m, u, 20)

p = plot(xrec, urec, label="t=5")
plot!(xrec, ic.(xrec))
plot!(xrec, dfdx_exact_fn.(xrec))
scatter!(solution_points(m), u[:])

err_list
p = plot()
title!("Convergence of Flux Derivative")
colors = distinguishable_colors(length(order_list), RGB(1.0,.75,0))
for o = 1:length(order_list)
    x,y,m = err_reg(nel_list, err_list[:,o])
    scatter!(nel_list, err_list[:,o], xscale=:log10, yscale=:log10, label=string("el order=", order_list[o], " (slope= ",@sprintf("%.2f",m) ,")"), markercolor = colors[o])
    plot!(x, y, label="", linecolor=colors[o])
end
xlabel!("Number of Elements")
ylabel!("Relative Error")
display(p)
savefig("Flux_convergence")

x,y,m = err_reg(nel_list, err_list[:,3])
plot(x,y)
display(p)

