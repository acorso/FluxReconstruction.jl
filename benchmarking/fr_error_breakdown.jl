using FluxReconstruction
using Plots
# Plots of the relative and absolute error of the solution and derivative fo the solution as the simulation progresses


# Below are two versions fo the rk step and rk solve functions that return the values at all intermediate soultion points.
function rk_step_allpts(u, f, t, dt, scheme::ButcherTableau)
    uret,tret = [],[]
    k = [f(u,t)]
    for i=1:scheme.stages-1
        ust = u + dt*sum(k[1:i].*scheme.a[i,1:i])
        tst = t + scheme.c[i]*dt
        push!(uret, ust)
        push!(tret, tst)
        push!(k, f(ust, tst))
    end
    println(scheme.b')
    ul = u + dt*sum(k.*scheme.b')
    tl = t + dt
    push!(uret, ul)
    push!(tret, tl)
    uret, tret
end

function rk_solve_allpts(u0, f, dt, t_end, scheme::ButcherTableau)
    u,t = [u0],[0.]
    c = 0
    while t[end] < t_end
        if c % 100 == 0
            println("t=", t[end])
        end
        us, ts = rk_step_allpts(u[end], f, t[end], dt, scheme)
        push!(u, us...)
        push!(t, ts...)
        c += 1
    end
    u,t
end

order = 3
nel = 40
L = 2.0
dt = 0.001
Nit = 3
Tend = Nit*dt
v = 0.3747589;

m = LagrangeMesh((-L/2.0,), (L/2.0,), (nel,), LegendreElement(order,1))
xm = solution_points(m)
ps = ProblemStatement((Periodic, nothing), [(u) -> -v*u], [(uL, uR) -> -v*uL])

ex_sol_fn(x,t) = sin.(pi*(x+1-v*t))
enf(x) = ex_sol_fn(x,Tend)
ex_sol(t) = ex_sol_fn(xm, t)
ic(x) = ex_sol_fn(x, 0)

u0 = sample(ic, m)
uf = sample(enf, m)

dfdx_exact(t) = -v*pi*cos.(pi*(xm + 1 - v*t))
dfdx_num = create_divf(m, ps, 1)

p = plot()
for s = [MidpointMethod, RK4]
    u_num, t_num = rk_solve_allpts(u0, dfdx_num, dt, Tend, s)
    t_num
    N = length(t_num)
    abs_sol_err = zeros(N-1)
    abs_dfdx_err = zeros(N-1)
    for i=2:N
        u,t = u_num[i], t_num[i]
        abs_sol_err[i-1] = mean(abs.(u[:] .- ex_sol(t)))
        abs_dfdx_err[i-1] = mean(abs.(dfdx_num(u,t)[:] .- dfdx_exact(t)))
    end
    t_num
    abs_sol_err
    abs_dfdx_err

    plot!(t_num[2:end], abs_sol_err, label=string("Solution Error - ", s.name), yscale=:log10, marker = (:hexagon, 5))
    plot!(t_num[2:end], abs_dfdx_err, label=string("Derivative Error - ", s.name), marker = (:hexagon, 5))
end
display(p)

