using FluxReconstruction
using StaticArrays
# Time a largish mesh
m = LagrangeMesh((0,0,0), (1,1,1), (5,5,5), LegendreElement(4,3))
ic(x,y,z) = [exp(x^2 + y^2 + z^2),1,1,1,1,1,1,1]
u = sample(ic,m)
divf = Array{Float64}(size(u))

adv_f(a::Float64) = f(u) = a*u
int_f(a::Float64) = f(uL, uR) = a*uL
fluxes = [adv_f(1.), adv_f(2.), adv_f(3.)]
interface_fluxes = [int_f(1.), int_f(2.), int_f(3.)]
ps = ProblemStatement(3, (Dirichlet, dirichlet(@MArray ones(8))), fluxes, interface_fluxes)

# baseline time: (    0.330507 seconds (955.53 k allocations: 59.783 MiB, 5.26% gc time)   )
divf!(divf, m, ps, u, 1.0)
@time divf!(divf, m, ps, u, 1.0)


