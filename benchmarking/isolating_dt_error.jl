using FluxReconstruction
using Plots

L = 20
T = 24
ps = ProblemStatement((Dirichlet, ((t)->sin(pi*t/2), neumann(0.) )), [(u) -> -u], [(uL, uR) -> -uL])
order = 3
nel = 30

dt_list = [0.00202303202, 0.0012029233, 0.0005101, 0.000202, 0.000101101932, 0.0000501101932]
# dt_list = [0.1]

u_res = Array{Array{Float64},1}(length(dt_list))
err_list = Array{Float64,1}(length(dt_list))
meshes = Array{LagrangeMesh,1}(length(dt_list))
for t = 1:length(dt_list)
    dt = dt_list[t]
    m = LagrangeMesh((0,), (L,), (nel,), LegendreElement(order,1))
    xsol = solution_points(m)
    u = rk_solve(sample((x)->0,m), create_divf(m, ps, 1), dt, T, MidpointMethod)[1][end]
    err = mean(abs.(u[:] + sin.(pi*xsol/2)))
    println("err: ", err)

    # Store the results
    u_res[t], meshes[t], err_list[t] = u, m, err

    println("done")
end
err_list
u = u_res[4]
m = meshes[4]

urec, xrec = reconstruct(m, u, 20)

x = linspace(0,20,1000);
p1 = plot(x, -sin.(pi*x/2), label="Initial Condition")
plot!(xrec, urec, label="t=5")
scatter!(solution_points(m), u[:])

p = plot()
title!("Convergence in dt")
x,y,m = err_reg(dt_list, err_list)
scatter!(dt_list, err_list, xscale=:log10, yscale=:log10, xflip=true)
plot!(x, y, label="")
display(p)

nel = [10., 30.]
err = [0.023, 0.00017]
err_reg(nel, err)

