using FluxReconstruction
using Plots

## Convergence in 1D
order = 5
xpts = [linspace(-1,1,order+1)...]
ypts = rand(order+1)

p1 = lagrange_polynomial(xpts, ypts)
p2 = lagrange_polynomial(xpts, rand(order+1))
pcomb(x) = [p1(x), p2(x)]

orders = [1:order...]
ires = Array{Float64,2}(length(orders), 2)
for o in orders
    ires[o,:] = quad_integrate(pcomb, o, 1)
end

pl1 = plot(orders, ires)
title!(string("1D Quad (Order~", order, ")"))
xlabel!("X")
ylabel!("Y")

## 2D example
order = 5
xpts = [linspace(-1,1,order+1)...]
ypts = rand((order+1)^2)

p1 = lagrange_polynomial(xpts, ypts, 2)
p2 = lagrange_polynomial(xpts, rand((order+1)^2), 2)
pcomb2(x1, x2) = [p1(x1, x2), p2(x1,x2)]
pcomb2(1.0, 1.0)
orders = [1:order...]
ires = Array{Float64,2}(length(orders), 2)
for o in orders
    ires[o,:] = quad_integrate(pcomb2, o, 2)
end
pl2 = plot(orders, ires)
title!(string("2D Quad (Order~", order, ")"))
xlabel!("X")
ylabel!("Y")

display(plot(pl1, pl2))

