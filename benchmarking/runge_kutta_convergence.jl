using FluxReconstruction
using Plots
using LaTeXStrings

schemes = [ForwardEuler, MidpointMethod, HeunsMethod, RalstonMethod, RK4, ThreeEighthsRule]

## Solution for the First order linear DE u' = -2 u (u = u0 e^-2t)
f(u, t) = -2.*u
u0 = 1.
u_exact(t) = u0*exp.(-2*t)

dt = .1
tend = 3

# Plot all of the solutions
p1 = plot(linspace(0,tend,1000), u_exact, label="Exact Solution")
for scheme in schemes
    u_num, t_num = rk_solve(u0, f, dt, tend, scheme)
    plot!(t_num, hcat(u_num...)', label=scheme.name, marker = 2)
end
xlabel!("Time")
ylabel!("Y")
title!(L"Numerical Solutions to ODE $y' = 2y$")

p2 = plot()
colors = distinguishable_colors(length(schemes), RGB(1.0,.75,0))
for i in 1:length(schemes)
    err, dts, = rk_convergence(u0, f, dt, tend, schemes[i], u_exact)
    x,y,m = err_reg(dts, err)
    p2 = scatter!(dts, err, label=string(schemes[i].name," (slope= ",@sprintf("%.2f",m) ,")"), xscale = :log10, xflip=true, yscale = :log10, markercolor = colors[i])
    plot!(x,y, label="",linecolor = colors[i], legend=:bottomleft)
end
xlabel!(L"\Delta t")
ylabel!("Relative Error")
title!("Convergence History")

p = plot(p1,p2)
display(p)
