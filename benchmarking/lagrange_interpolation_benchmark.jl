using FluxReconstruction

xpts = [0, 0.25, 0.5, 0.75, 1]
ypts = [1 2 5 -1 0.5]
x = [linspace(0,1,1000)...]

"Function that explicity performs all the calculations needed to interpolated
the provided sample points"
function pfast(x)
    1.*(x-0.25)*(x-.5)*(x-0.75)*(x-1.)/((0-0.25)*(0-.5)*(0-0.75)*(0-1)) + 2*(x-0.)*(x-.5)*(x-0.75)*(x-1.)/((0.25-0.)*(0.25-.5)*(0.25-0.75)*(0.25-1)) + 5*(x-0.)*(x-0.25)*(x-0.75)*(x-1.)/((0.5-0)*(0.5-0.25)*(0.5-0.75)*(0.5-1)) +
    -1*(x-0.)*(x-0.25)*(x-.5)*(x-1.)/((0.75-0)*(0.75-0.25)*(0.75-.5)*(0.75-1)) +
    0.5*(x-0.)*(x-0.25)*(x-.5)*(x-0.75)/((1.-0.)*(1.-0.25)*(1.-.5)*(1.-0.75))
end

## Test the lagrange interpolation function compared to the fast version
println("======== Testing Lagrange Polynomial Function Eval Speed ========")
pfast.(x)
println("Fast function evaluation (hard-coded):")
@time pfast.(x)

p = lagrange_polynomial(xpts, ypts)
p.(x)
println("Lagrange polynomial function evaluation (flexible):")
@time p.(x)

dp = lagrange_polynomial_derivative(xpts,ypts)
dp.(x)
println("Lagrange polynomial derivative evaluation:")
@time dp.(x)
println("====================== Done =========================")

## Test the interpolation matrix compared to function evaluation
println("======== Testing Interpolation Matrix Speedup =========")
m = interp_matrix(xpts, x)
dm = deriv_interp_matrix(xpts, x)

println("Function Evaluation Speed (interpolation then derivative):")
@time p.(x)
@time dp.(x)

println("Interpolation matrix speed (interpolation then derivative):")
@time ypts*m
@time ypts*dm
println("====================== Done =========================")

## Previous run:
# ====================== Done =========================
# ======== Testing Lagrange Polynomial Function Eval Speed ========
# Fast function evaluation (hard-coded):
#   0.000117 seconds (29 allocations: 9.125 KiB)
# Lagrange polynomial function evaluation (flexible):
#   0.000783 seconds (5.04 k allocations: 384.344 KiB)
# Lagrange polynomial derivative evaluation:
#   0.001873 seconds (63.03 k allocations: 1.260 MiB)
# ====================== Done =========================
# ======== Testing Interpolation Matrix Speedup =========
# Function Evaluation Speed (interpolation then derivative):
#   0.000638 seconds (5.04 k allocations: 384.344 KiB)
#   0.001701 seconds (63.03 k allocations: 1.260 MiB)
# Interpolation matrix speed (interpolation then derivative):
#   0.000016 seconds (5 allocations: 8.094 KiB)
#   0.000009 seconds (5 allocations: 8.094 KiB)
# ====================== Done =========================
