using FluxReconstruction
using Plots
# function for solving the advection equation using the upwind difference method

function create_dfdx_uw(x, flux)
    dx = x[2]-x[1]
    function df(u, t)
        f = flux.(u)
        (f - [flux(sin(pi*t/2)), f[1:end-1]... ])/dx
    end
end

L = 20
T = 24
dt = 0.001

x = linspace(0,L, 400)
u0 = zeros(size(x))
uwadf = create_dfdx_uw(x, (u) -> -u)
uwadf(u0, 0.0)
u_rk4 = rk_solve(u0, uwadf, dt, T, RK4)[1][end]
u_midpoint = rk_solve(u0, uwadf, dt, T, MidpointMethod)[1][end]

p = plot(x, u_rk4, label="UW - RK4")
plot!(x, u_midpoint, label="UW - MidpointMethod")

ps = ProblemStatement((Dirichlet, ((t)->sin(pi*t/2), neumann(0.) )), [(u) -> -u], [(uL, uR) -> -uL])
m = LagrangeMesh((0,), (L,), (100,), LegendreElement(3,1))
u0_fr = sample((x)->0,m)
fradf = create_divf(m, ps, 1)

u_fr_rk4 = rk_solve(u0_fr, fradf, dt, T, RK4)[1][end]
u_fr_midpoint = rk_solve(u0_fr, fradf, dt, T, MidpointMethod)[1][end]

urec_rk4, xrec = reconstruct(m, u_fr_rk4, 4)
urec_midpoint, xrec = reconstruct(m, u_fr_midpoint, 4)

plot!(xrec, urec_rk4, label="FR - RK4")
plot!(xrec, urec_midpoint, label="FR - MidpointMethod")
title!("FR vs. Linear Upwind")
xlabel!("x")
ylabel!("y")

savefig("comp")
display(p)

