using FluxReconstruction
using Plots

# Check that the 1D flux looks correct
m = LagrangeMesh((0,), (50,), (50,), InterfaceLegendreElement(4,1))
ic(x) = exp(-(x - 20)^2)
u = sample(ic,m)
divf = 10*ones(size(u))

adv_f(a::Float64) = adv_f(u) = a*u
int_f(a::Float64) = int_f(uL, uR) = a*uL
fluxes = [adv_f(-1.)]
interface_fluxes = [int_f(-1.)]
ps = ProblemStatement((Neumann, neumann(0.)), fluxes, interface_fluxes)
divf!(divf, m, ps, u, 1.0)

df, x = reconstruct(m, divf, 5)
urec, x = reconstruct(m,u, 5)

p1 = plot(x, urec, label="u")
plot!(x, df, label="df/dx")
xlabel!("x")
ylabel!("u or df/dx")
title!("1D Solution and Divergence \n (Legendre Element)")



# Check that the 2D flux looks correct
m = LagrangeMesh((0,0), (50,50), (20,20), LobattoElement(4,2))
ic(x,y) = exp(-0.1(x - 20)^2 - 0.05(y-20)^2)
u = sample(ic,m)
divf = Array{Float64}(size(u))

fluxes = [adv_f(-1.), adv_f(-5.)]
interface_fluxes = [int_f(-1.), int_f(-5.)]
ps = ProblemStatement(2, (Neumann, neumann(0.)), fluxes, interface_fluxes)

divf!(divf, m, ps, u, 1.0)

df, x1, x2 = reconstruct(m, divf, 10)
urec, x1, x2 = reconstruct(m, u, 10)

p2 = plot(x1, x2, urec', label="u")
plot!(x1, x2, df', label="df/dx")

xlabel!("x")
ylabel!("u or df/dx")
title!("2D Solution and Divergence \n (Lobatto Element)")

pf = plot(p1,p2)
display(pf)

