using FluxReconstruction
using Plots

m = LagrangeMesh((0,0), (50,50), (12,10), LegendreElement(4,2))
adv_f(a::Float64) = adv_f(u) = a*u
int_f(a::Float64) = int_f(uL, uR) = a*uL
fluxes, int_fluxes = [adv_f(-1.), adv_f(-1.)], [int_f(-1.), int_f(-1.)]
ps = ProblemStatement(2, (Neumann, neumann(0.)), fluxes, int_fluxes)

adf2 = create_divf(m, ps, 1)

ic(x,y) = exp(-0.1(x - 20)^2 -0.1(y - 20)^2 )
u0 = sample(ic,m)

u,t = rk_solve(u0, adf2, 0.01, 1, RK4)

u0rec, x1, x2 = reconstruct(m, u0, 20)
urec, x1, x2 = reconstruct(m, u[101], 20)

p = plot(x1, x2, u0rec')
plot!(x1, x2, urec')
display(p)

