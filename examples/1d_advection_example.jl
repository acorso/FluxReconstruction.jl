using FluxReconstruction
using Plots

m = LagrangeMesh((0,), (50,), (200,), LegendreElement(4,1))
adv_f(a::Float64) = adv_f(u) = a*u
int_f(a::Float64) = int_f(uL, uR) = a*uL
fluxes, int_fluxes = [adv_f(-1.)], [int_f(-1.)]
ps = ProblemStatement((Neumann, neumann(0.)), fluxes, int_fluxes)

adf = create_divf(m, ps, 1)

ic(x) = exp(-0.1(x - 20)^2)
u0 = sample(ic,m)
adf(u0, 1.0)
u,t = rk_solve(u0, adf, 0.01, 5, RK4)

u0rec, x = reconstruct(m, u0, 20)
urec, x = reconstruct(m, u[502], 20)

p = plot(x, u0rec, label="Initial Condition")
plot!(x, urec, label="t=5")

display(p)

