using FluxReconstruction
using Plots

#1D
L = 30;
m = LagrangeMesh((0,), (L,), (10,), LobattoElement(4,1))
ps = ProblemStatement(((Dirichlet, Neumann), (0,-3)))

K, Fbase, me, LM, dir_mask = assemble_fem(m, ps)
x = solution_points(m)
bforce = reshape(sin.(2*pi*x / L), size(LM)...)
sol = solve_diffusion(K, Fbase, me, LM, dir_mask, m, bforce)

u = [sol(a,e) for a=1:prod(m.element.npts), e=1:prod(m.nel)];
urec, xrec = reconstruct(m, u, 10)
p1 = scatter(x, u[:], label="solution points")
plot!(xrec, urec, label="reconstructed solution")
title!("FEM with right neumann = -3")
xlabel!("X")
ylabel!("Y")

# 2D
m = LagrangeMesh((0,0), (30,30), (10,11), InterfaceLegendreElement(3,2))
x = solution_points(m)
ps = ProblemStatement(((Dirichlet, (10,0)), (Neumann, (0, 10))))

K, Fbase, me, LM, dir_mask = assemble_fem(m, ps)
bforce = Array{Float64,1}(size(x,2))
for i=1:length(bforce)
    bforce[i] = sin(2*pi*x[1,i]/30)*sin(2*pi*x[2,i]/30)
end
sol = solve_diffusion(K, Fbase, me, LM, dir_mask, m, reshape(bforce, size(LM)))

x1 = solution_points(m)
s = [sol(a,e) for a=1:prod(m.element.npts), e=1:prod(m.nel)]

ux, x1 = reconstruct(m, s, (:,2), (:,1), 10)
uy, x2 = reconstruct(m, s, (2,:), (1,:), 10)
p2 = plot(x1, ux, label="X-slice (dir, 10,0)")
plot!(x2, uy, label="Y-slice (neu, (0,10))")
title!("2D Slices ")
xlabel!("X")
ylabel!("Y")

p = plot(p1,p2)
display(p)

