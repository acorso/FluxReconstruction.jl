using FluxReconstruction
using Plots

# Integrate using a 3D mesh
m = LagrangeMesh((0,0,0), (1,1,1), (10,11,12), LegendreElement(4,3))
f(x,y,z) = x + y^2 + z^3
u = sample(f,m)

u1 = allocate_with_less_dims(size(u,1), m.element.npts, m.nel, 2, 3)
integrate!(u1, u, m, 2, 3)
u1_ex(x) = x + 7/12
xx = solution_points(m, 1)
scatter(xx, u1[:])
plot!(xx, u1_ex.(xx))

u2 = allocate_with_less_dims(size(u,1), m.element.npts, m.nel, 1, 3)
integrate!(u2, u, m, 1, 3)
u2_ex(y) = y^2 + 3/4
yy = solution_points(m, 2)
scatter(yy, u2[:])
plot!(yy, u2_ex.(yy))

u3 = allocate_with_less_dims(size(u,1), m.element.npts, m.nel, 3)
integrate!(u3, u, m, 3)
u3_ex(x,y) = x + y^2 + 1/4
xy = solution_points(m, 1,2)
scatter(xy[1,:], xy[2,:], u3[:])
surface!(xy[1,:], xy[2,:], [u3_ex(yy[:,i]...) for i=1:size(yy,2)] )

