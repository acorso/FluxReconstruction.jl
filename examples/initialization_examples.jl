using FluxReconstruction
using Plots

m1 = LagrangeMesh((0,), (1,), (10,), LegendreElement(3,1))
f(x) = x*x
u = sample(f, m1)
x_sol = solution_points(m1)
p1 = scatter(x_sol, u[:], label="")
title!("1D Sample Points for x^2")

m2 = LagrangeMesh((0,0), (1,1), (10,11), LobattoElement(4,2))
f2(x,y) = x^2 + y^2
u2 = sample(f2, m2)
x_sol_2 = solution_points(m2)
p2 = scatter(x_sol_2[1,:], x_sol_2[2,:], u2[:], label="")
title!("2D Sample points for x^2 + y^2")

p = plot(p1,p2)
display(p)

