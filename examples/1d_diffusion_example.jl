using FluxReconstruction
using Plots

L = 30;
m = LagrangeMesh((0,), (L,), (10,), LobattoElement(4,1))
ps = ProblemStatement((Dirichlet, 0))

K, Fbase, me, LM, dir_mask = assemble_fem(m, ps)
x = solution_points(m)
bforce = reshape(sin.(2*pi*x / L), size(LM)...)
sol = solve_diffusion(K, Fbase, me, LM, dir_mask, m, bforce)

u = [sol(a,e) for a=1:prod(m.element.npts), e=1:prod(m.nel)];
urec, xrec = reconstruct(m, u, 10)
p = scatter(x, u[:], label="solution points")
plot!(xrec, urec, label="reconstructed solution")

x_ex = linspace(0,L,1000)
u_ex = (L^2/(4*pi^2))*sin.(2*pi*x_ex/L)
plot!(x_ex, u_ex, label="exact solution")
title!("FEM Solution of 1D Diffusion")
xlabel!("X")
ylabel!("Y")

display(p)

