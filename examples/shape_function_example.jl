using FluxReconstruction
using Plots
using Jacobi

nsol = 3

xsol_leg = [-1.0, legendre_zeros(nsol)..., 1.0]
xsol_lob = [zglj(nsol+2, 0,0)...]
xplot = [linspace(-1,1,1000)...]

l_leg = lagrange_basis(xsol_leg)
l_lob = lagrange_basis(xsol_lob)
dl_leg = lagrange_basis_derivative(xsol_leg)
dl_lob = lagrange_basis_derivative(xsol_lob)
N_leg = hcat(l_leg.(xplot)...)
N_lob = hcat(l_lob.(xplot)...)
dN_leg = hcat(dl_leg.(xplot)...)
dN_lob = hcat(dl_lob.(xplot)...)

p1 = scatter(xsol_leg, zeros(size(xsol_leg)), label="Legendre Points")
scatter!(xsol_lob, zeros(size(xsol_lob)), label="Lobatto Points")
plot!(xplot, N_leg', label="", color = :blue)
plot!(xplot, N_lob', label="", color = :red)
title!("Shape Functions")
xlabel!("X")
ylabel!("Y")


p2 = scatter(xsol_leg, zeros(size(xsol_leg)), label="Legendre Points")
scatter!(xsol_lob, zeros(size(xsol_lob)), label="Lobatto Points")
plot!(xplot, dN_leg', color = :blue, label="")
plot!(xplot, dN_lob', color = :red, label="")
title!("Shape Function Deriv")
xlabel!("X")
ylabel!("Y'")

p = plot(p1,p2)
display(p)

