using FluxReconstruction
using Plots

m = LagrangeMesh((0,0,0), (30,30,30), (7,7,7), LegendreElement(2,3))
adv_f(a::Float64) = adv_f(u) = a*u
int_f(a::Float64) = int_f(uL, uR) = a*uL
int_fR(a::Float64) = int_f(uL, uR) = a*uR
fluxes = [adv_f(-1.), adv_f(-1.), adv_f(1.)]
int_fluxes = [int_f(-1.), int_f(-1.), int_fR(1.)]
ps = ProblemStatement(3, (Neumann, neumann(0.)), fluxes, int_fluxes)

adf3 = create_divf(m, ps, 1)

ic(x,y, z) = exp(-0.1(x - 20)^2 - 0.1(y - 20)^2 - 0.1(z - 20)^2 )
u0 = sample(ic,m)

u,t = rk_solve(u0, adf3, 0.01, 1, RK4)

u0rec_xy, x1, x2 = reconstruct(m, u0, (:,:,1), (:,:,4), 20)
urec_xy, x1, x2 = reconstruct(m, u[101], (:,:,1), (:,:, 4), 20)

p1 = plot(x1, x2, u0rec_xy')
plot!(x1, x2, urec_xy')
title!("X-Y Cross Section")

u0rec_yz, x1, x2 = reconstruct(m, u0, (1,:,:), (4,:,:), 20)
urec_yz, x1, x2 = reconstruct(m, u[101], (1,:,:), (4,:,:), 20)

p2 = plot(x1, x2, u0rec_yz')
plot!(x1, x2, urec_yz')
title!("Y-Z Cross Section")

p = plot(p1, p2)
display(p)

