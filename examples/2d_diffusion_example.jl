using FluxReconstruction
using Plots

m = LagrangeMesh((0,0), (30,30), (10,11), InterfaceLegendreElement(3,2))
x = solution_points(m)
ps = ProblemStatement(((Dirichlet, (10,0)), (Periodic, nothing)))


K, Fbase, me, LM, dir_mask = assemble_fem(m, ps)
bforce = Array{Float64,1}(size(x,2))
for i=1:length(bforce)
    bforce[i] = sin(2*pi*x[1,i]/30)*sin(2*pi*x[2,i]/30)
end
LM
sol = solve_diffusion(K, Fbase, me, LM, dir_mask, m, reshape(bforce, size(LM)))

x1 = solution_points(m)
s = [sol(a,e) for a=1:prod(m.element.npts), e=1:prod(m.nel)]

u, x1, x2 = reconstruct(m, s, 10)
p = surface(x1, x2, u')

display(p)

