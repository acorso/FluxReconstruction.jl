using FluxReconstruction
using Plots

m = LagrangeMesh((0,-1,-2), (1,2,3), (2,3,4), LobattoElement(4,3))
f_exp(x,y,z) = e^(-4.*(x^2 + y^2 + z^2))
u = sample(f_exp, m)

# 1D reconstruction
urec1d, x1 = reconstruct(m,u, (3,4,:), (1,2,:), 20)
xsol1d = solution_points(m, 3)
p1 = scatter(xsol1d, u[:,3,4,:,1,2,:][:])
plot!(x1, urec1d)
title!("1D Sample Across X \n (LobattoElement)")

# 2D reconstruction
m = LagrangeMesh((0,-1,-2), (1,2,3), (2,3,4), LobattoElement(4,3))
f_exp(x,y,z) = e^(-4.*(x^2 + y^2 + z^2))
u = sample(f_exp, m)
urec2d, x1, x2 = reconstruct(m, u, (1,:,:), (1,:,:), 5)
xsol2d = solution_points(m, 2, 3)
p2 = scatter(xsol2d[1,:], xsol2d[2,:], label="")
plot!(x1, x2, urec2d', label="")
title!("2D Sample Across Y and Z \n (Legendre Element)")


p = plot(p1,p2)
display(p)

