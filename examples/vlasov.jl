using Plots
using FluxReconstruction

D = 1
V = 1
order = 4

xdims = [1:D...]
vdims = [D+1:D+V...]

xmin = zeros(D)
xmax = ones(D)
vmin = -5*ones(V)
vmax = 5*ones(V)
xel = 10*ones(Int, D)
vel = 20*ones(Int, D)

m = LagrangeMesh((xmin...,vmin...), (xmax..., vmax...), (xel..., vel...), LegendreElement(4, V+D))

ic(x,v) = 0.5*(exp(-10*(v-1)^2) + exp(-10*(v+1)^2))

fvec = sample(ic, m)
rhovec = allocate_with_less_dims(1,m.element.npts,m.nel, vdims...)
integrate!(rhovec, fvec, m, vdims...)

fplot, x, v = reconstruct(m, fvec, 20)
contour(x,v,fplot')


