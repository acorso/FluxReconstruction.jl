using FluxReconstruction
using Plots

## Solution to coupled first order ode
# u' = 3u + 4v
# v' = -4u + 3v

rk_example_f(u,t) = [3 4; -4 3]*u;
y_exact(t) = [sin(4t)*exp(3t), cos(4t)*exp(3t)]
u0 = [0.,1.]
dt = .05
t_end = 5.

u_num, t_num = rk_solve(u0, rk_example_f, dt, t_end, RK4)
u_ex = hcat(y_exact.(t_num)...)

p = plot(t_num, hcat(u_num...)', label="Numerical Solution")
plot!(t_num, u_ex', label="Exact Solution", legend=:topleft)
title!("Solution to First-Order Coupled ODE")
display(p)

