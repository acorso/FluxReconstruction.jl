using FluxReconstruction
using Plots

## Example 1D lagrange interpolation (functional, matrix, derivative)
xpts = [0, 0.25, 0.5, 0.75, 1]*5
ypts = [1, 2, 5, -1, 0.5]

p = lagrange_polynomial(xpts, ypts)
dp = lagrange_polynomial_derivative(xpts, ypts)
dp(1.0)
x = [linspace(0,5,100)...]
m = interp_matrix(xpts, x)
dm = deriv_interp_matrix(xpts, x)

p1 = plot(x, p.(x), label="Lagrange Poly Function")
plot!(x, (ypts'*m)', label="Interpolation Matrix")
plot!(x, dp.(x), label="Derivative Poly Function")
plot!(x, (ypts'*dm)', label="Derivative Interpolation Matrix")
scatter!(xpts, ypts, markersize=5, label="Sample Points")
title!("1D Interpolation")

## Example of 2D interpolation
zpts = rand(25)
p2 = lagrange_polynomial(xpts, zpts, 2)
dp2 = lagrange_polynomial_derivative(xpts, zpts, 2)
xpts_2d = broadcast_points(xpts, 2)

dp2(-1.,-1.)

N = 20
x = [linspace(0,5,N)...]
x_broad = reshape([[x1, x2] for x1 in x, x2 in x], (N^2,1))
x_interp = hcat(x_broad...)
s = surface(x_interp[1,:], x_interp[2,:], [p2.(x1, x2) for x1 in x, x2 in x][:], label="Interpolating Surface")
scatter!(xpts_2d[1,:], xpts_2d[2,:], zpts, label="Sample Points")
title!("2D Interpolation")

ds1 = plot(x_interp[1,1:20], [p2.(x1, 3.) for x1 in x][:], label="Interpolating Polynomial")
plot!(x_interp[1,1:20], [dp2.(x1, 3.)[1] for x1 in x][:], label="dP/dx")
title!("Derivative in the X-Direction")
xlabel!("X")
ylabel!("Y")

ds2 = plot(x_interp[1,1:20], [p2.(3., x1) for x1 in x][:], label="Interpolating Polynomial")
plot!(x_interp[1,1:20], [dp2.(3., x1)[2] for x1 in x][:], label="dP/dy")

title!("Derivative in the Y-Direction")
xlabel!("X")
ylabel!("Y")

pl1 = plot(p1, s)
pl2 = plot(ds1,ds2)

display(plot(pl1, pl2, layout = (2,1)))


